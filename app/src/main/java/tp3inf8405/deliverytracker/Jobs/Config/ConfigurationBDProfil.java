package tp3inf8405.deliverytracker.Jobs.Config;

import android.app.Activity;
import android.util.Log;

import com.path.android.jobqueue.JobManager;
import com.path.android.jobqueue.config.Configuration;
import com.path.android.jobqueue.log.CustomLogger;

/**
 * Project: DeliveryTracker
 * Created by TP2 on 2016-03-02.
 * Description: Configuration du thread qui va ouvrir la BD locale pour charger le profil
 *              Dans la liste de profil
 */


public class ConfigurationBDProfil {
    Configuration configuration_;
    JobManager jobManager_;


    public ConfigurationBDProfil(Activity T){
        configuration_ = new Configuration.Builder(T)
                .customLogger (new CustomLogger() {
                    private static final String TAG = "DispalyProfile: LoadingAProfileTrips";
                    @Override
                    public boolean isDebugEnabled() {
                        return true;
                    }
                    @Override
                    public void d(String text, Object... args) {
                        Log.d(TAG, String.format(text, args));
                    }
                    @Override
                    public void e(Throwable t, String text, Object... args) {
                        Log.e(TAG, String.format(text, args), t);
                    }
                    @Override
                    public void e(String text, Object... args) {
                        Log.e(TAG, String.format(text, args));
                    }
                })
                .minConsumerCount(1)//always keep at least one consumer alive
                .maxConsumerCount(3)//up to 3 consumers at a time
                .loadFactor(3)//3 jobs per consumer
                .consumerKeepAlive(120)//wait 2 minute
                .id("EditProfile")
                .build();
        jobManager_ = new JobManager(T, configuration_);
    }

    public JobManager getJobManager() {
        return jobManager_;
    }
}
