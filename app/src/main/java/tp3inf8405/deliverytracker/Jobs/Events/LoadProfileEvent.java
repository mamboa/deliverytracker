package tp3inf8405.deliverytracker.Jobs.Events;

import tp3inf8405.deliverytracker.DB.MembreModel;

/**
 * Project: MeetUp
 * Created by TP2 on 2016-03-06.
 * Description: Evenement utilisé pour transférer le profil utilisateur vers le
 */
public class LoadProfileEvent {
    private MembreModel membre_;
    private boolean empty_;

    public LoadProfileEvent() {
        empty_ = true;
        membre_ = null;
    }

    public boolean isEmpty() {
        return empty_;
    }

    public LoadProfileEvent setEmpty(boolean empty_) {
        this.empty_ = empty_;
        return this;
    }

   public MembreModel getMembre() {
        return membre_;
    }

    public LoadProfileEvent setMembre(MembreModel membre_) {
        this.membre_ = membre_;
        if(this.membre_ != null)
            this.empty_ = false;
        return this;
    }
}


