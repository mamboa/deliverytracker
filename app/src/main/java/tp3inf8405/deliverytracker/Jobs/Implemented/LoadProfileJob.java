package tp3inf8405.deliverytracker.Jobs.Implemented;

import android.content.Context;

import com.path.android.jobqueue.Job;
import com.path.android.jobqueue.Params;

import java.util.ArrayList;

import tp3inf8405.deliverytracker.Bus.MainThreadBus;
import tp3inf8405.deliverytracker.DB.DeliveryTrackerDAO;
import tp3inf8405.deliverytracker.DB.MembreModel;
import tp3inf8405.deliverytracker.Jobs.Events.LoadProfileEvent;


public class LoadProfileJob extends  Job{
    public static final int PRIORITY = 1;
        DeliveryTrackerDAO dbDAO_= null;

    public LoadProfileJob(Context context){
            super(new Params(PRIORITY));
            dbDAO_ = new DeliveryTrackerDAO(context);
    }

    @Override
    public void onAdded() {

    }

    @Override
    public void onRun() throws Throwable {
        //on charge le profil et on le transmet à DisplayProfile.java
        ArrayList<MembreModel> membres = new ArrayList<MembreModel>();
        membres.addAll(dbDAO_.getAllMembre());
        if(membres.size() > 0)
            MainThreadBus.getDefault().post(new LoadProfileEvent().setMembre(membres.get(0)));
        else
            MainThreadBus.getDefault().post(new LoadProfileEvent());
    }

    @Override
    protected void onCancel() {

    }

    @Override
    protected boolean shouldReRunOnThrowable(Throwable throwable) {
            return false;
    }

 }


