package tp3inf8405.deliverytracker.DB;

import com.google.android.gms.maps.model.LatLng;


public class MembreModel {
    private int id;  //id local dans la bd
    private String idFirebase_;
    private String nom_;
    private String courriel_;
    private LatLng location_;
    private String idFacebook_;
    private String lienImage_;

    public MembreModel()
    {
        this.id = 0;
        this.nom_ = "";
        this.idFirebase_ = "";
        this.idFacebook_ = "";
        this.courriel_ = "";
        this.location_ = null;
        this.lienImage_ = "";
    }

    public MembreModel(MembreModel membre){
        if (membre != null) {
            this.nom_ = membre.getNom();
            this.idFirebase_ = membre.getIdFirebase();
            this.courriel_ = membre.getCourriel();
            this.location_ = membre.getLocation();
            this.idFacebook_ = membre.getIdFacebook();
            this.lienImage_ = membre.getLienImage();
        }
    }
    public int getId() {
        return id;
    }

    public MembreModel setId(int id) {
        this.id = id;
        return this;
    }

    public String getNom() {
        return nom_;
    }

    public MembreModel setNom(String nom) {
        this.nom_ = nom;
        return this;
    }

    public String getIdFirebase() {
        return idFirebase_;
    }

    public MembreModel setIdFirebase(String idMembre) {
        this.idFirebase_ = idMembre;
        return this;
    }

    public String getCourriel() {
        return courriel_;
    }

    public MembreModel setCourriel(String courriel_) {
        this.courriel_ = courriel_;
        return this;
    }

    public LatLng getLocation() {
        return location_;
    }

    public MembreModel setLocation(LatLng location_) {
        this.location_ = location_;
        return this;
    }

    public String getIdFacebook() {
        return idFacebook_;
    }

    public MembreModel setIdFacebook(String idFacebook) {
        this.idFacebook_ = idFacebook;
        return this;
    }

    public String getLienImage() {
        return lienImage_;
    }

    public MembreModel setLienImage(String lienImage) {
        this.lienImage_ = lienImage;
        return this;
    }
}
