package tp3inf8405.deliverytracker.DB;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
/**
 * Project: DeliveryTracker
 * Created by TP3 on 2016-03-02.
 * Description:
 */
public class DeliveryTrackerDBDAO {
    protected SQLiteDatabase database;
    private DeliveryTrackerDBHelper dbHelper_;
    private Context mContext_;

    public DeliveryTrackerDBDAO(Context context) {
        this.mContext_ = context;
        dbHelper_ = DeliveryTrackerDBHelper.getHelper(mContext_);
        open();
    }

    public void open() throws SQLException {
        if(dbHelper_ == null)
            dbHelper_ = DeliveryTrackerDBHelper.getHelper(mContext_);
        database = dbHelper_.getWritableDatabase();
    }
}