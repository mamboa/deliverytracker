package tp3inf8405.deliverytracker.DB;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Project: DeliveryTracker
 * Created by TP3 on 2016-03-02.
 * Description:
 */
public class DeliveryTrackerDBHelper extends SQLiteOpenHelper {

    //Database name
    //mal necessaire
    public static String DBNAME = "meetUp";
    //version number of the database
    private static int VERSION = 1;
    //attributs de la table profil
    public static final String ID = "id";
    public static final String ID_FIREBASE = "id_firebase";
    public static final String ID_FACEBOOK = "id_facebook";
    public static final String COURRIEL = "courriel";
    public static  final String LOCATION = "location";
    public static final String NOM = "nom";
    public static final String LIEN_IMAGE = "lien_image";
    //attributs de la table groupe
   /* public static final String ID_GROUPE_2 ="id";
    public static final String ID_FIREBASE ="idFirebase";
    public static final String NOM = "nom";*/


    //name of the table
    public static final String DATABASE_TABLE = "profil";
   // public static final String DATABASE_TABLE_GROUPE = "groupe";

    //an instance of SQLiteDatabase
    private static DeliveryTrackerDBHelper myInstance_;

    public DeliveryTrackerDBHelper(Context context){
        super(context, DBNAME, null, VERSION);
    }

    public static synchronized DeliveryTrackerDBHelper getHelper(Context context) {
        if (myInstance_ == null)
            myInstance_ = new DeliveryTrackerDBHelper(context);
        return myInstance_;
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
    /*
        String sqlGroupe = "create table if not exists " + DATABASE_TABLE_GROUPE + " ( " +
                ID_GROUPE_2 + " integer primary key autoincrement , " +
                NOM + " TEXT, " +
                ID_FIREBASE + " TEXT " +
                " ) ";*/
        String sqlProfil =     "create table if not exists " + DATABASE_TABLE + " ( " +
                ID + " integer primary key autoincrement , " +
                ID_FIREBASE + " TEXT , " +
                ID_FACEBOOK + " TEXT , " +
                NOM + " TEXT ," +
                COURRIEL + " TEXT , " +
                LIEN_IMAGE + " TEXT , " +
                LOCATION + " TEXT  " +
                " ) ";

       // db.execSQL(sqlGroupe);
        db.execSQL(sqlProfil);
    }


    private static final int DATABASE_VERSION = 1; // indicate database update

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // drop the old table
        db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE);
     //   db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE_GROUPE);
        //Create the table again
        onCreate(db);
    }
}
