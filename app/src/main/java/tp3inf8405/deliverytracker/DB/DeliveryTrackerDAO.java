package tp3inf8405.deliverytracker.DB;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

import tp3inf8405.deliverytracker.Converters.Converter;

/**
 * Project: DeliveryTracker
 * Created by TP3 on 2016-03-02.
 * CRUD class (create read update delete)
 */
public class DeliveryTrackerDAO extends DeliveryTrackerDBDAO {

    private static final String WHERE_ID_EQUALS = DeliveryTrackerDBHelper.ID;
    private static final String WHERE_IDFACEBOOK_EQUALS = DeliveryTrackerDBHelper.ID_FACEBOOK;
    private static Context context_;
    public DeliveryTrackerDAO(Context context) {
        super(context);
        context_ = context;
    }

    /**
     * insertion d'un membre
     * @param membre
     * @return identifiant de l'endroit où le membre a été inséré
     */
    public long insert(MembreModel membre){
        ContentValues contentValues = new ContentValues();
        contentValues.put(DeliveryTrackerDBHelper.ID_FIREBASE, membre.getIdFirebase());
        contentValues.put(DeliveryTrackerDBHelper.ID_FACEBOOK,membre.getIdFacebook());
        contentValues.put(DeliveryTrackerDBHelper.NOM,membre.getNom());
        contentValues.put(DeliveryTrackerDBHelper.COURRIEL, membre.getCourriel());
        contentValues.put(DeliveryTrackerDBHelper.LIEN_IMAGE, membre.getLienImage());
        contentValues.put(DeliveryTrackerDBHelper.LOCATION, Converter.converter_.latlongToString(membre.getLocation()));
        return database.insert(DeliveryTrackerDBHelper.DATABASE_TABLE, null, contentValues);
    }

    /**
     * obtenir tous les membres()
     * @return
     */
    public ArrayList<MembreModel> getAllMembre() {
        ArrayList<MembreModel> membres = new ArrayList<MembreModel>();

        Cursor cursor = database.query(DeliveryTrackerDBHelper.DATABASE_TABLE,
                new String[] { DeliveryTrackerDBHelper.ID,
                        DeliveryTrackerDBHelper.ID_FIREBASE,
                        DeliveryTrackerDBHelper.ID_FACEBOOK,
                        DeliveryTrackerDBHelper.NOM,
                        DeliveryTrackerDBHelper.COURRIEL,
                        DeliveryTrackerDBHelper.LIEN_IMAGE,
                        DeliveryTrackerDBHelper.LOCATION
                        }, null, null, null, null, DeliveryTrackerDBHelper.ID +" DESC" ,null);

        while (cursor.moveToNext()) {
            int i= 0;
            MembreModel membre_ = new MembreModel()
                    .setId(cursor.getInt(0))
                    .setIdFirebase(cursor.getString(1))
                    .setIdFacebook(cursor.getString(2))
                    .setNom(cursor.getString(3))
                    .setCourriel(cursor.getString(4))
                    .setLienImage(cursor.getString(5))
                    .setLocation(Converter.converter_.stringToLatLng(cursor.getString(6)));
            membres.add(membre_);
        }
        cursor.close();
        if (membres.size() == 0)
            membres = null;
        return membres;
    }





    /**
     * obtenir un membre
     * @param id identifiant local du membre
     * @return le membre
     *Todo: Ne fonctionne pas, à débogguer
     */
    public MembreModel getMembre(long id) {
        MembreModel membre_ = null;
        String sql = "SELECT * FROM " + DeliveryTrackerDBHelper.DATABASE_TABLE
                + " WHERE " + DeliveryTrackerDBHelper.ID + " = ?";
        //bind the request to a cursor
        Cursor cursor = database.rawQuery(sql, new String[] { String.valueOf(id) });
        //collect the data
        if (cursor.moveToNext()) {
             membre_ = new MembreModel()
            .setId(cursor.getInt(0))
            .setIdFirebase(cursor.getString(1))
            .setIdFacebook(cursor.getString(2))
            .setNom(cursor.getString(3))
            .setCourriel(cursor.getString(4))
            .setLienImage(cursor.getString(5))
            .setLocation(Converter.converter_.stringToLatLng(cursor.getString(6)));
        }
        cursor.close();
        return membre_;
    }

    /**
     * obtenir un membre
     * @param facebookId identifiant facebook local du membre
     * @return le membre
     *Todo: Ne fonctionne pas, à débogguer
     */
    public MembreModel getMembre(String facebookId) {
        MembreModel membre_ = null;
        String sql = "SELECT * FROM " + DeliveryTrackerDBHelper.DATABASE_TABLE
                + " WHERE " + DeliveryTrackerDBHelper.ID + " = ?";
        //bind the request to a cursor
        Cursor cursor = database.rawQuery(sql, new String[] { String.valueOf(facebookId) });
        //collect the data
        if (cursor.moveToNext()) {
            membre_ = new MembreModel()
                    .setId(cursor.getInt(0))
                    .setIdFirebase(cursor.getString(1))
                    .setIdFacebook(cursor.getString(2))
                    .setNom(cursor.getString(3))
                    .setCourriel(cursor.getString(4))
                    .setLienImage(cursor.getString(5))
                    .setLocation(Converter.converter_.stringToLatLng(cursor.getString(6)));
        }
        cursor.close();
        return membre_;
    }

    /**
     * delete() effacer un membre
     * @param membre
     * @return id l'identifiant de l'élément supprimé
     */
    public int delete(MembreModel membre) {
        return database.delete(DeliveryTrackerDBHelper.DATABASE_TABLE, WHERE_IDFACEBOOK_EQUALS + "=?",
                new String[] { membre.getIdFacebook() + "" });
    }


    /**
     * update() mise à jour d'un membre
     * @param membre
     * @return the id of the updated row
     */
    public long update(MembreModel membre) {
        long result = -1;
        if (membre != null){
            ContentValues values = new ContentValues();
            values.put(DeliveryTrackerDBHelper.ID_FIREBASE, membre.getIdFirebase());
            values.put(DeliveryTrackerDBHelper.ID_FACEBOOK, membre.getIdFacebook());
            values.put(DeliveryTrackerDBHelper.NOM, membre.getNom());
            values.put(DeliveryTrackerDBHelper.COURRIEL, membre.getCourriel());
            values.put(DeliveryTrackerDBHelper.LIEN_IMAGE, membre.getLienImage());
            values.put(DeliveryTrackerDBHelper.LOCATION, Converter.converter_.latlongToString(membre.getLocation()));

            result = database.update(DeliveryTrackerDBHelper.DATABASE_TABLE, values,
                    WHERE_ID_EQUALS + "=?",
                    new String[]{String.valueOf(membre.getId()) + ""});
        }
        return result;
    }

    /**
     * savoir si un utilisateur existe (on ne peut avoir plus de deux courriels par compte)
     */
    public  boolean siMembreExiste(String courriel){
        return valueExistsInDb(DeliveryTrackerDBHelper.DBNAME, DeliveryTrackerDBHelper.DATABASE_TABLE, DeliveryTrackerDBHelper.ID_FACEBOOK, courriel);
    }

    /**
     * savoir si un enregistrement existe
     */
    public static boolean valueExistsInDb(String dbName,String table, String field, String value) {
        SQLiteDatabase sqldb = context_.openOrCreateDatabase(dbName, Context.MODE_PRIVATE, null);
        boolean existe = DatabaseUtils.longForQuery(sqldb, "select count(*) from " + table + " where " + field + "=? limit 1", new String[]{value}) > 0;
        sqldb.close();
        return existe;
    }

    /**
     * obtenir une valeur d'un champ dans une colonne donnée
     * @param dbName
     * @param table
     * @param field
     * @param value
     * @return
     */
    public Object getAValue(String dbName,String table, String field, String value){
        SQLiteDatabase sqldb = context_.openOrCreateDatabase(dbName, Context.MODE_PRIVATE, null);
        Object valeur = DatabaseUtils.longForQuery(sqldb, "select * from " + table + " where " + field + "=? limit 1", new String[]{value}) > 0;
        sqldb.close();
        return valeur;
    }
}
