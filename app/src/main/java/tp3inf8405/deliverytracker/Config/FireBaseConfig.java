package tp3inf8405.deliverytracker.Config;

/**
 * Project: DeliveryTracker
 * Created by TP3 on 2016-03-02.
 */
public class FireBaseConfig {
    public static final String FIREBASE_URL = "https://glowing-inferno-8938.firebaseio.com/";
}
