package tp3inf8405.deliverytracker.Config;

/**
 * Project: DeliveryTracker
 * Created by TP3 on 2016-03-08.
 * Description: certaines fonctions utiles, propres au fonctionnement de l'application
 */

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.BatteryManager;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static java.util.Locale.getDefault;

/**
 * obtenir la derniere position GPS connue
 * source:http://stackoverflow.com/questions/6694391/android-get-current-location-of-user-without-using-gps-or-internet
 */
public class Utils {
    public static Location getLastKnownLoaction(boolean enabledProvidersOnly, Context context){
        LocationManager manager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        Location utilLocation = null;
        List<String> providers = manager.getProviders(enabledProvidersOnly);
        for(String provider : providers){
            utilLocation = manager.getLastKnownLocation(provider);
            if(utilLocation != null) return utilLocation;
        }
        return null;
    }

    /**
     * retourne le niveau de la batterie
     * source: http://developer.android.com/training/monitoring-device-state/battery-monitoring.html
     * @return le niveau actuel de la batterie en pourcentage
     */
    public static float niveauDeBatterie(Context context) {
        Intent batteryIntent =  context.registerReceiver(null, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
        int level = batteryIntent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
        int scale = batteryIntent.getIntExtra(BatteryManager.EXTRA_SCALE, -1);
        if(level == -1 || scale == -1) {
            return 50.0f;
        }
        return ((float)level / (float)scale) * 100.0f;
    }

    /**
     * Affiche une erreur à  l'écran
     * @param T Activité courante
     * @param message le message d'erreur
     */
    public static void erreurDialogue(Activity T, String message) {
        new AlertDialog.Builder(T)
                .setTitle("Erreur")
                .setMessage(message)
                .setPositiveButton(android.R.string.ok, null)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    public static String dateEtHeureActuelle(){
        SimpleDateFormat sdf2 = new SimpleDateFormat("HH:mm");
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("EEE dd-MM-yyy");
        return sdf2.format(cal.getTime())+" "+sdf.format(new Date());
    }

    /**
     * getCompleteAddresString() gives the complete address of a location point whose lat and long
     * are given
     * source: http://stackoverflow.com/questions/9409195/how-to-get-complete-address-from-latitude-and-longitude
     * @param LATITUDE of the point
     * @param LONGITUDE of the point
     * @return
     */
    public static String getCompleteAddressString(double LATITUDE, double LONGITUDE, Activity activity_) {
        String strAdd = "";
        Geocoder geocoder = new Geocoder(activity_, getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
            if (addresses != null) {
                Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder("");

                for (int i = 0; i < returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                }
                strAdd = strReturnedAddress.toString();
                Log.w("My Current loction address", "" + strReturnedAddress.toString());
            } else {
                Log.w("My Current loction address", "No Address returned!");
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.w("My Current loction address", "Canont get Address!");
        }
        return strAdd;
    }


}