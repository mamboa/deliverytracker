package tp3inf8405.deliverytracker.Config;

import retrofit.client.Response;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Project: DeliveryTracker
 * Created by TP3 on 2016-03-15.
 * Description: Interface de requete retrofit. Facilite les requetes http vers le serveur de
 *              Google place Api
 */
public interface PlaceApiRequestInterface {

    @GET("/maps/api/place/nearbysearch/json")

    //synch request: all wait on the same thread
    public Response getJson(@Query("location") String location,
                            @Query("radius") String radius,
                            @Query("language") String language,
                         //   @Query("rankby") String rankby,
                            @Query("type") String types,
                            @Query("key") String key);
}
//https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=-33.8670522,151.1957362&radius=500&type=restaurant&name=cruise&key=YOUR_API_KEY
//        &key=AIzaSyBExgECnAK5bGpt3avMwnx6UhzIw7_Ur70
