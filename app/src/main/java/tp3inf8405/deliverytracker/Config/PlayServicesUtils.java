package tp3inf8405.deliverytracker.Config;

import android.app.Activity;
import android.app.Dialog;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

/**
 * Project: DeliveryTracker
 * Created by TP3 on 2016-03-04.
 * Description: Interroge l'appareil pour savoir si une version de google play service est à jour
 *              retourne un message d'erreur adéquat
 */
public class PlayServicesUtils {
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    public static boolean checkGooglePlaySevices(final Activity activity) {
        GoogleApiAvailability googleAPI = GoogleApiAvailability.getInstance();
        final int googlePlayServicesCheck = googleAPI.isGooglePlayServicesAvailable(activity);
        switch (googlePlayServicesCheck) {
            case ConnectionResult.SUCCESS:
                return true;
            case ConnectionResult.SERVICE_DISABLED:
            case ConnectionResult.SERVICE_INVALID:
            case ConnectionResult.SERVICE_MISSING:
            case ConnectionResult.SERVICE_VERSION_UPDATE_REQUIRED:
                Dialog dialog = googleAPI.getErrorDialog(activity, PLAY_SERVICES_RESOLUTION_REQUEST, 0);
                dialog.setOnCancelListener(new android.content.DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(android.content.DialogInterface dialogInterface) {
                        activity.finish();
                    }
                });
                dialog.show();
        }
        return false;
    }
}
