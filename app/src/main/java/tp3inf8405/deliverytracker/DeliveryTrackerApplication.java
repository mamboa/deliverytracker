package tp3inf8405.deliverytracker;

import android.app.Application;
import android.content.Context;

import com.facebook.FacebookSdk;
import com.google.firebase.database.FirebaseDatabase;

/**
 * Project: DeliveryTracker
 * Created by TP3 on 2016-03-02.
 * Description: Application class
 */
public class DeliveryTrackerApplication extends Application {
    private static Context mContext;
    private static DeliveryTrackerApplication mInstance;
    @Override
    public void onCreate() {
        super.onCreate();
        FirebaseDatabase.getInstance().setPersistenceEnabled(true);

        FacebookSdk.sdkInitialize(this);
        mContext = this;
        mInstance = this;
    }

    public static Context getContext(){
        return mContext;
    }
    public static synchronized DeliveryTrackerApplication getInstance() {
        return mInstance;
    }
}
