package tp3inf8405.deliverytracker.Panier.framework;

import android.os.Handler;

import java.util.ArrayList;

import tp3inf8405.deliverytracker.Panier.database.DatabaseManager;
import tp3inf8405.deliverytracker.Panier.home.data.Product;

import static tp3inf8405.deliverytracker.Panier.util.LogUtils.LOGD;

/**
 * * MVP: - Model architecture
 * <p>
 * Created by b_ashish on 23-Feb-16.
 */
public class CartInteractorImpl implements CartInteractor {


    private static final String TAG = CartInteractorImpl.class.getSimpleName();

    @Override
    public void saveItemInCart(Object item, OnCartResponseListener listener) {

        boolean persistProduct = DatabaseManager.getInstance().saveCartProduct((Product) item);
        if (persistProduct) {
            LOGD(TAG, "Item added into local database");
            listener.onSuccess();
        } else {
            LOGD(TAG, "Item not added into local database");
            listener.onError();
        }
    }

    @Override
    public void removeItemFromCart(Object item, OnCartResponseListener listener) {

        boolean deleteCartProduct = DatabaseManager.getInstance().deleteCartProduct((Product) item);
        if (deleteCartProduct) {
            LOGD(TAG, "Item successfully deleted from local database");
            listener.onSuccessfulRemoved((Product) item);
        } else {
            LOGD(TAG, "Item not deleted from local database");
            listener.onError();
        }
    }

    @Override
    public void getTotalItemsInCart(final OnCartResponseListener listener) {
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                ArrayList<Product> cartProducts = DatabaseManager.getInstance().getCartProducts();
                listener.onDisplayItemsOnCart(cartProducts);
            }
        });
    }

    @Override
    public void countTotalItems(OnCartResponseListener listener) {
        int productCount = DatabaseManager.getInstance().getProductCount();
        listener.onDisplayTotalItem(productCount);
    }

    @Override
    public void getCategoryItems(final OnCartResponseListener listener, final int categoryId) {
        new Handler().post(new Runnable() {
            @Override
            public void run() {

                ArrayList<Product> cartProducts = DatabaseManager.getInstance().getCartProducts();
                ArrayList<Product> productsByCategory = DatabaseManager.getInstance().getProductsByCategory(categoryId);

                int position = 0;
                for (Product product : productsByCategory) {
                    if (!cartProducts.contains(product)) {
                        productsByCategory.get(position).isInCart = false;
                        LOGD(TAG, "item not matched");
                    } else {
                        productsByCategory.get(position).isInCart = true;
                        LOGD(TAG, "item matched");
                    }
                    position++;
                }
                listener.onDisplayCategoryItems(productsByCategory);
            }
        });
    }

    @Override
    public void emptyItemFromCart(OnCartResponseListener listener){
        boolean deleteCartProduct = DatabaseManager.getInstance().emptyCartProduct();
        if (deleteCartProduct) {
            LOGD(TAG, "Items successfully deleted from local database");
            listener.onSuccessfulCleared();
        } else {
            LOGD(TAG, "Items not deleted from local database");
            listener.onError();
        }
    }
}
