package tp3inf8405.deliverytracker.Panier.framework;

import java.util.List;

public interface OnCartResponseListener {

    void onSuccess();

    void onError();

    void onSuccessfulRemoved(Object item);

    void onDisplayItemsOnCart(List items);

    void onDisplayCategoryItems(List items);

    void onDisplayTotalItem(int size);

    void onSuccessfulCleared();
}
