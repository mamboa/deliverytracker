package tp3inf8405.deliverytracker.Panier.ui;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import tp3inf8405.deliverytracker.Panier.CommonConstants;
import tp3inf8405.deliverytracker.Panier.home.data.Product;
import tp3inf8405.deliverytracker.Panier.util.ImageLoader;
import tp3inf8405.deliverytracker.R;

import static tp3inf8405.deliverytracker.Panier.util.LogUtils.LOGD;
import static tp3inf8405.deliverytracker.Panier.util.LogUtils.makeLogTag;

public class AddCartDialogActivity extends tp3inf8405.deliverytracker.Panier.ui.BaseActivity implements OnClickListener {

    private static final String TAG = makeLogTag(AddCartDialogActivity.class);

    public static final String SELECTED_PRODUCT_ITEM = "tp3inf8405.deliverytracker.Panier.ui.SELECTED_PRODUCT_ITEM";

    public static final String CART_STATUS = "tp3inf8405.deliverytracker.Panier.ui.CART_STATUS";

    private Product selectedItem;
    private Button cartButt;

    public static final int REQUEST_TO_ADD_IN_CART = 1;
    private ImageLoader mImageLoader;

    enum MyCart {
        ADD_TO_CART(
                0) {
            @Override
            public String action() {
                return "Ajouter au panier";
            }
        },
        IN_CART(
                1) {
            @Override
            public String action() {
                return "Dans le panier";
            }
        },
        UPDATE_CART(
                2) {
            @Override
            public String action() {
                return "Mise à jour";
            }
        };
        private int value;

        public abstract String action();

        private MyCart(int value) {
            this.value = value;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_custom);
        mImageLoader = new ImageLoader(this, R.drawable.default_logo);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            selectedItem = extras.getParcelable(SELECTED_PRODUCT_ITEM);
        }

        setTitle("");

        cartButt = (Button) findViewById(R.id.cartButt);
        cartButt.setOnClickListener(this);

        ImageView btnClose = (ImageView) findViewById(R.id.btn_Close);
        btnClose.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                AddCartDialogActivity.this.finish();
            }
        });

        TextView itemTitle = (TextView) findViewById(R.id.title_page);
        TextView itemName = (TextView) findViewById(R.id.itemName); //description here
        TextView itemPrice = (TextView) findViewById(R.id.itemPrice);
        if (selectedItem != null) {
            itemTitle.setText(selectedItem.name);
            itemName.setText(selectedItem.description);
            String decimalString = CommonConstants.getDecimalString(selectedItem.price);
            itemPrice.setText("Prix: " + decimalString+"$");
            mImageLoader.loadAssetsImage(this, Uri.parse("file:///android_asset/product/" + selectedItem.imageUrlMedium), (ImageView) findViewById(R.id.productImage));
        }

        displayCartButtBehavior();
    }

    private void displayCartButtBehavior() {
        if (selectedItem.isInCart) {
           // cartButt.setEnabled(false);
            cartButt.setTag(MyCart.IN_CART);
            cartButt.setText(MyCart.IN_CART.action());
            cartButt.setTextColor(Color.WHITE);
        } else {
            cartButt.setEnabled(true);
            cartButt.setText(MyCart.ADD_TO_CART.action());
            cartButt.setTag(MyCart.ADD_TO_CART);
            cartButt.setTextColor(Color.WHITE);
        }
    }

    private void saveProduct() {
        cartPresenter.addItem(selectedItem);
    }

    @Override
    public void onProductActionResponse(boolean isActionSuccessful) {
        super.onProductActionResponse(isActionSuccessful);
        if (isActionSuccessful) {
            Toast.makeText(this, "Ajouté au panier.", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent();
            intent.putExtra(CART_STATUS, true);
            setResult(RESULT_OK, intent);
            finish();
        } else {
            Toast.makeText(this, "Erreur à l'insertion des données.", Toast.LENGTH_SHORT).show();
            LOGD(TAG, "Item not added into local database");
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.cartButt:
                MyCart mycart = (MyCart) cartButt.getTag();
                switch (mycart) {
                    case ADD_TO_CART:
                        LOGD(TAG, "Ajout au panier");
                        saveProduct();
                        break;
                    case IN_CART:
                        LOGD(TAG, "Dans le panier");
                        finish();
                        break;
                    case UPDATE_CART:
                        LOGD(TAG, "Mise à jour du panier");
                        break;
                }
                break;
            default:
                break;
        }
    }

    @Override
    protected void onDestroy() {
        cartPresenter.onDestroy();
        super.onDestroy();
    }
}
