package tp3inf8405.deliverytracker.Panier.cart;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;


import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import mehdi.sakout.fancybuttons.FancyButton;
import tp3inf8405.deliverytracker.Config.Utils;
import tp3inf8405.deliverytracker.Converters.Converter;
import tp3inf8405.deliverytracker.DB.DeliveryTrackerDAO;
import tp3inf8405.deliverytracker.DB.MembreModel;
import tp3inf8405.deliverytracker.Panier.home.data.Product;
import tp3inf8405.deliverytracker.Panier.ui.BaseActivity;
import tp3inf8405.deliverytracker.Panier.ui.widget.DrawShadowFrameLayout;
import tp3inf8405.deliverytracker.Panier.util.LogUtils;
import tp3inf8405.deliverytracker.R;



/**
 * Created by ashish (Min2) on 06/02/16.
 * adapted by Maxime
 */
public class CartActivity extends BaseActivity {

    private static final String TAG = LogUtils.makeLogTag(CartActivity.class);

    private double tps = 0.05;
    private double tvq =0.09975;
    private String dateHeureCourante = "";
    private double coutTotal = 0.0;
    private String facture = "";
    @Bind(R.id.drawer_layout)
    DrawerLayout mDrawerLayout;
    private CartFragment mFragment;
    private FancyButton btn_commander_;
    DeliveryTrackerDAO profilDAO_;

    MembreModel membreCourant_  = null;
    Context context_;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cart_act);
        ButterKnife.bind(this);
        context_ = this.getBaseContext();

        profilDAO_ = new DeliveryTrackerDAO(this.getApplicationContext());
        if(profilDAO_.getAllMembre() != null )
            membreCourant_ = profilDAO_.getAllMembre().get(0);//MainThreadBus.getDefault().getStickyEvent(MembreModel.class);

        btn_commander_ = (FancyButton)findViewById(R.id.btn_commander);
        btn_commander_.setRadius(10);
        btn_commander_.setText("Passer la commande");
        btn_commander_.setTextSize(20);
        btn_commander_.setTextColor(Color.parseColor("#FFFFFF"));
        btn_commander_.setFocusBackgroundColor(Color.parseColor("#ff4eadd8"));
        btn_commander_.setBackgroundColor(Color.parseColor("#007ab8"));

        comportementBoutonCommander();

        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow_flipped, GravityCompat.END);

        registerHideableHeaderView(findViewById(R.id.headerbar));
        mFragment = (CartFragment) getFragmentManager().findFragmentById(R.id.product_request_frag);

        // Add the back button to the toolbar.
        Toolbar toolbar = getActionBarToolbar();
        toolbar.setNavigationIcon(R.drawable.ic_up);
        toolbar.setNavigationContentDescription(R.string.close_and_go_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                navigateUpOrBack(CartActivity.this, null);
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout != null && mDrawerLayout.isDrawerOpen(GravityCompat.END)) {
            mDrawerLayout.closeDrawer(GravityCompat.END);
        } else {
            super.onBackPressed();
        }
    }


    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        enableActionBarAutoHide((ListView) findViewById(R.id.collection_view));
    }


    @Override
    protected void onActionBarAutoShowOrHide(boolean shown) {
        super.onActionBarAutoShowOrHide(shown);
        DrawShadowFrameLayout frame = (DrawShadowFrameLayout) findViewById(R.id.main_content);
        frame.setShadowVisible(shown, shown);
    }

    @Override
    protected void onDestroy() {
        cartPresenter.onDestroy();
        super.onDestroy();
    }

    @Override
    public void displayCartItems(List items) {
        super.displayCartItems(items);
        mFragment.refreshData(items);
    }

    @Override
    public void onSuccessfulDeletion(Object item) {
        super.onSuccessfulDeletion(item);
        mFragment.removeData(item);
    }

    @Override
    public void onSuccessfulClearance() {
        super.onSuccessfulClearance();
        mFragment.removeAllData();
    }

    public void comportementBoutonCommander() {
        btn_commander_.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(membreCourant_ != null){
                    dialogBoxAjoutNom();
                }
                else{
                    Toast.makeText(context_, "Vous devez vous connecter pour pouvoir effectuer des commandes. Merci ", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    /**
     * dessine la boîte de dialoge qui s'ouvre lorsque l'utilisateur clique sur ajouter
     */
    void dialogBoxAjoutNom(){
        //etablissement de la facture
        //coût total
        initialiser();
        String boissons = "";
        String pizzas = "";
        //on recupere la liste des elements du panier
        List<Product> produits = new ArrayList<Product>();
        final CartFragment.CartListAdapter adapter = mFragment.getmAdapter();
        for (int i = 0; i < adapter.getCount(); i++) {
            produits.add(adapter.getItem(i));
            if (getCategorie(produits.get(i).categoryId).equals("Pizzas"))
                pizzas += produits.get(i).name + " " + produits.get(i).price + "$\n";
            else
                boissons += produits.get(i).name + " " + produits.get(i).price + "$\n";
            coutTotal += Double.parseDouble(produits.get(i).price);
        }

        //la date et l'heure actuelle
        dateHeureCourante = Utils.dateEtHeureActuelle();
        facture += dateHeureCourante + "\n";
        if (!pizzas.equals("")) {
            facture += "PIZZAS:\n" + pizzas;
        }
        if (!boissons.equals("")) {
            facture += "BOISSONS:\n" + boissons;
        }

        facture += "Total avant taxes " + String.format("%.2f", coutTotal) + "$\n";
        facture += "TPS " + String.format("%.2f", coutTotal * tps) + "$\n";
        facture += "TVQ " + String.format("%.2f", coutTotal * tvq) + "$\n";
        facture += "Total après taxes " + String.format("%.2f", coutTotal * (1 + tvq + tps)) + "$\n";
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);

        //on affiche une boîte de dialogue pour confirmer la commander
        builder.setMessage(facture);
        builder.setTitle("Détails de la commande");
        builder.setPositiveButton("Valider", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.cancel();
                if(adapter.getCount() == 0){
                    Toast.makeText(getApplicationContext(), "Le panier est vide", Toast.LENGTH_SHORT).show();
                }
                else {
                    //Affichage du niveau de batterie
                   Toast.makeText(getApplicationContext(),"Niveau de la batterie: "+ Utils.niveauDeBatterie(getApplicationContext())+"% .", Toast.LENGTH_LONG).show();
                    //sauvegarder la commande en local et en ligne(avec keepsynced)
                    DatabaseReference lienCommande = FirebaseDatabase.getInstance().getReference().child("commandes");
                    DatabaseReference commandeActuelle = lienCommande.push();
                    commandeActuelle.keepSynced(true);
                    Map<String, String> commande = new HashMap<String, String>();
                    commande.put("facebook_id_client", membreCourant_.getIdFacebook());
                    commande.put("nom_client",membreCourant_.getNom());
                    commande.put("heure", dateHeureCourante);
                    commande.put("position_client", Converter.converter_.latlongToString(Converter.converter_.locationToLatLng(Utils.getLastKnownLoaction(true,context_))));
                    commande.put("details", facture);
                    commande.put("etat","En attente"); //en attente / en cours de livraison/ livré*/
                    commande.put("montant", coutTotal+"$");
                    commande.put("heure_depart","");
                    commande.put("facebook_id_livreur","");
                    commande.put("nom_livreur","");
                    commande.put("position_livreur","");
                    commandeActuelle.setValue(commande);
                    Toast.makeText(context_, "Votre commande est en cours de traitement. Merci ", Toast.LENGTH_SHORT).show();

                    // vider le panier
                    cartPresenter.emptyTheCart();

                    //on ferme le panier
                    finish();
                }
            }
        });
        builder.setNegativeButton("Annuler", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }


    public String getCategorie(int a){
        if(a == 1 )
            return  "Pizzas";
        if(a == 2 )
            return  "Boissons";
        return "erreur";
    }

    public void initialiser(){
        coutTotal = 0.0;
        facture = "";
        dateHeureCourante = "";
    }

}
