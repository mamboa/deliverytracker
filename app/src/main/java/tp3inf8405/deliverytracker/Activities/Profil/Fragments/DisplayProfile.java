package tp3inf8405.deliverytracker.Activities.Profil.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.facebook.login.widget.ProfilePictureView;

import tp3inf8405.deliverytracker.DB.DeliveryTrackerDAO;
import tp3inf8405.deliverytracker.DB.MembreModel;
import tp3inf8405.deliverytracker.R;


/**
 * Project: MeetUp
 * Created by TP2 on 2016-03-05.
 * Description:
 */
public class DisplayProfile extends Fragment {

    DeliveryTrackerDAO profilDAO_;
    MembreModel membreCourant_ = null;
    TextView nom_;
    TextView courriel_;

    private ToggleButton toggleButton;

    ProfilePictureView profilePictureView;

    public DisplayProfile() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        profilDAO_ = new DeliveryTrackerDAO(getActivity());
        if(profilDAO_.getAllMembre() != null )
            membreCourant_ = profilDAO_.getAllMembre().get(0);//MainThreadBus.getDefault().getStickyEvent(MembreModel.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View myInflatedView = inflater.inflate(R.layout.display_profile_fragment, container,false);

        nom_  = (TextView)myInflatedView.findViewById(R.id.nom);
        courriel_ = (TextView)myInflatedView.findViewById(R.id.courriel);
        toggleButton = (ToggleButton)myInflatedView.findViewById(R.id.mode_livreur);

        profilePictureView = (ProfilePictureView) myInflatedView.findViewById(R.id.image);

        return myInflatedView;
    }

    @Override
    public void onStart(){
        super.onStart();
        //si les données de facebook sont dispos, il y connexion on les charge
        if(membreCourant_ != null){
            //id: (String)mAuthData.getProviderData().get("id")
            nom_.setText(membreCourant_.getNom());
            courriel_.setText(membreCourant_.getCourriel());
            // on affiche l'image de l'utilisateur en utilisant son id facebook
            profilePictureView.setProfileId(membreCourant_.getIdFacebook());
        }
        else{
            Toast.makeText(getActivity(), "Vous devez vous connecter pour afficher le votre profil. Merci ", Toast.LENGTH_SHORT).show();
            getActivity().finish();
            return;
        }
    }

    @Override
    public void onStop(){
        super.onStop();
    }

    @Override
    public void onResume(){
        super.onResume();
    }


    public void onToggleClicked(View view) {
        // Is the toggle on?
        boolean on = ((ToggleButton) view).isChecked();

        if (on) {
            // Enable vibrate
        } else {
            // Disable vibrate
        }
    }
}