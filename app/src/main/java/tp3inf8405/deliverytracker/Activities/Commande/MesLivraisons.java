package tp3inf8405.deliverytracker.Activities.Commande;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.path.android.jobqueue.JobManager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import mehdi.sakout.fancybuttons.FancyButton;
import tp3inf8405.deliverytracker.Activities.MapsActivity;
import tp3inf8405.deliverytracker.Bus.MainThreadBus;
import tp3inf8405.deliverytracker.Config.PlayServicesUtils;
import tp3inf8405.deliverytracker.Config.Utils;
import tp3inf8405.deliverytracker.Converters.Converter;
import tp3inf8405.deliverytracker.DB.DeliveryTrackerDAO;
import tp3inf8405.deliverytracker.DB.MembreModel;
import tp3inf8405.deliverytracker.Jobs.Config.ConfigurationBDProfil;
import tp3inf8405.deliverytracker.POJO.Commande;
import tp3inf8405.deliverytracker.R;

/**
 * Project: DeliveryTracker
 * Created by TP3 on 2016-03-14.
 * Description:
 */
public class MesLivraisons extends AppCompatActivity implements
        LocationListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    private ListView listView_;

    private CommandeAdapter commandeAdpater_;
    private FancyButton btnAjouterCommande;
    private MembreModel membreCourant_;
    private DatabaseReference ref_;
    private String idCommandeTampon_ = "";
    private Query query_ = null;

    private Commande commandeASupprimer = null;
    //BD
    DeliveryTrackerDAO profilDAO_;
    JobManager jobManagerBDProfilUI_;
    ConfigurationBDProfil configurationBDProfilJob_;
    JobManager jobManagerPlacesOnline_;

    //AuthData mAuthData = null;
    LocationManager mLocationManager_;
    LocationRequest mLocationRequest_;
    GoogleApiClient mGoogleApiClient_;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.votelist_activity);
        //mAuthData = MainThreadBus.getDefault().getStickyEvent(AuthData.class);

        mGoogleApiClient_ = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        // permet de savoir si googleplay est disponible, montre une erreur au cas echeant
        PlayServicesUtils.checkGooglePlaySevices(this);
        mLocationManager_ = (LocationManager)this.getSystemService(Context.LOCATION_SERVICE);

        listView_ = (ListView) findViewById(R.id.list);
        View empty = findViewById(android.R.id.empty);
        //BD locale
        profilDAO_ = new DeliveryTrackerDAO(this);
        membreCourant_ = null;

        if(profilDAO_.getAllMembre() != null )
            membreCourant_ = profilDAO_.getAllMembre().get(0);

        if (membreCourant_ != null){
            MainThreadBus.getDefault().postSticky(membreCourant_);
            //FireBase
            ref_ = FirebaseDatabase.getInstance().getReference().child("deliverytracker");
            query_ = ref_.child("commandes")
                    .startAt(membreCourant_.getIdFacebook())
                    .endAt(membreCourant_.getIdFacebook())
                    .orderByChild("facebook_id_livreur");
            ref_.keepSynced(true);
            //2-
            commandeAdpater_ =  new CommandeAdapter(query_,this,R.layout.list_row);
            listView_.setAdapter(commandeAdpater_);

        } else {
            Toast.makeText(this, "Vous devez vous connecter pour voir vos Livraisons. Merci ", Toast.LENGTH_SHORT).show();
            finish();
            return;
        }
        if(commandeAdpater_.getCount() == 0)
            listView_.setEmptyView(empty);

        //boutton ajouter
        btnAjouterCommande = (FancyButton)findViewById(R.id.btn_ajouter);
        btnAjouterCommande.setRadius(10);
        //losrsqu'on clique sur un item de la liste
        listView_.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // on charge une nouvelle page en selectionnant un élément de la liste
                Commande commande =  (Commande) parent.getItemAtPosition(position);
                if(commande.getEtat().equals("En cours de livraison")) { // on affiche la position du livreur sur la carte
                    dialogBox((Commande) parent.getItemAtPosition(position));
                }
                else if(commande.getEtat().equals("En attente"))
                    Toast.makeText(getApplicationContext(), membreCourant_.getNom()+", aucun livreur n'a pris votre commande pour le moment, veuillez patienter. ", Toast.LENGTH_SHORT).show();
                else{
                    Toast.makeText(getApplicationContext(), "Cette commande a déjà été livrée ", Toast.LENGTH_SHORT).show();
                }
            }
        });
        //on longpress
        listView_.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view,
                                           int position, long id) {
                Commande commande =  (Commande) parent.getItemAtPosition(position);
                dialogBox(commande);
                return true;
            }
        });
        btnAjouterCommande = (FancyButton)findViewById(R.id.btn_ajouter);
        btnAjouterCommande.setVisibility(View.GONE);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        return super.onContextItemSelected(item);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient_.connect();
    }

    @Override
    protected void onStop() {
        stopLocationUpdates();
        // on deconnecte le client
        mGoogleApiClient_.disconnect();
        super.onStop();
    }


    /**
     * boite de dialogue qui s'affiche lorsqu'on longpress sur un evenement
     * permet d'afficher les détails d'une commande
     */
    void dialogBox(final Commande commande){
        commandeASupprimer = commande;
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        final TextView command = new TextView(this.getApplicationContext());
        command.setTextColor(Color.BLACK);

        builder.setMessage(commande.getDetails());

        builder.setView(command);
        //si la commande n'est pas livré on ne peut la supprimer
        if(commande.getEtat().equals("Livré")) {
            builder.setTitle("Suppression:");
            builder.setPositiveButton("Supprimer", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    dialog.cancel();
                    //suppression
                    DatabaseReference suppression = ref_.child("commandes").child(commande.getIdFirebase());
                    suppression.keepSynced(true);
                    suppression.removeValue();
                    Toast.makeText(getApplicationContext(), "Cette commande a été supprimée de l'historique", Toast.LENGTH_SHORT).show();
                }
            });
            builder.setNegativeButton("Annuler", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    dialog.cancel();
                }
            });
        }
        else if(commande.getEtat().equals("En cours de livraison")){
            String details = "";
            LatLng positionClient = Converter.converter_.stringToLatLng(commande.getPosition_client());
            details +="Adresse de destination: "+ Utils.getCompleteAddressString(positionClient.latitude, positionClient.longitude, this)+"\n\n";
            details += "Nom du livreur: " + commande.getNom_livreur()+"\n\n";
            builder.setMessage(details + commande.getDetails());
            builder.setPositiveButton("Ouvrir avec google maps", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    LatLng livreur = Converter.converter_.stringToLatLng(commande.getPosition_livreur());
                    LatLng client = Converter.converter_.stringToLatLng(commande.getPosition_client());
                    openInGoogleMaps(client,livreur);

                    Toast.makeText(getApplicationContext(), "Ouverture...", Toast.LENGTH_SHORT).show();
                }
            });
            builder.setNegativeButton("Annuler", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    dialog.cancel();
                }
            });
        }
        else{
            builder.setTitle("Détails:");
            String details = "";
            LatLng positionClient = Converter.converter_.stringToLatLng(commande.getPosition_client());
            details +="Adresse de destination: "+ Utils.getCompleteAddressString(positionClient.latitude, positionClient.longitude, this)+"\n\n";
            details += "Nom du livreur: " + commande.getNom_livreur();
            builder.setMessage(details + commande.getDetails());
            builder.setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    dialog.cancel();
                }
            });
        }

        AlertDialog alert = builder.create();
        alert.show();
    }

    public void openInGoogleMaps(LatLng client, LatLng livreur){
        String uri = String.format("http://maps.google.com/maps?saddr=%f,%f(%s)&daddr=%f,%f (%s)", livreur.latitude, livreur.longitude,"source" ,client.latitude, client.longitude,"destination");
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
        intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
        try
        {
            this.startActivity(intent);
        }
        catch(ActivityNotFoundException ex)
        {
            try
            {
                Intent unrestrictedIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                this.startActivity(unrestrictedIntent);
            }
            catch(ActivityNotFoundException innerEx)
            {
                Toast.makeText(getApplicationContext(), "Veuillez installer GoogleMaps.", Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onResume(){
        super.onResume();
    }

    void miseAjourComplete(){
        query_ = ref_.child("commandes")
                .orderByChild("facebook_id_livreur");
        query_.keepSynced(true);
        query_.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // Si la solution contient plusieurs enregistrements on les parcour
                if(dataSnapshot.getValue() != null) {
                    int test = 0;
                    ArrayList<Commande> commandes= new ArrayList<Commande>();
                    ArrayList<String> clefs= new ArrayList<String>();
                    for(DataSnapshot child: dataSnapshot.getChildren()) {
                        Commande celleci = child.getValue(Commande.class);
                        //on circonscrit les courriels
                        if(celleci!=null) {
                            if (celleci.getFacebook_id_livreur().equals(membreCourant_.getIdFacebook())) {
                                clefs.add(child.getKey());
                                commandes.add(celleci);
                            }
                        }

                    }
                    for(int i = 0 ; i < commandes.size(); i++){
                        if(commandes.get(i)!= null)
                        {
                            DatabaseReference com = ref_.child("commandes").child(clefs.get(i));
                            com.child("position_livreur").setValue(Converter.converter_.latlongToString(membreCourant_.getLocation()));
                        }
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError firebaseError) {
            }
        });
    }


    /**
     * Mise à jour des deux BD
     */
    void miseAjourPosition(Location location){
        membreCourant_.setLocation(Converter.converter_.locationToLatLng(location));

        //on met à jour la base de données locale
        profilDAO_.update(membreCourant_);

        //on met à jour FireBase (la base de données en ligne)

        DatabaseReference ref = ref_.child("users").child(membreCourant_.getIdFirebase());
        ref.keepSynced(true);
        miseAjourComplete();

        // mise à jour de la position
        Map<String, Object> updateUser = new HashMap<String,Object>();
        updateUser.put("position", Converter.converter_.latlongToString(Converter.converter_.locationToLatLng(location)));
        ref.updateChildren(updateUser);
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        createLocationRequest();
        startLocationUpdates();
    }

    @Override
    public void onConnectionSuspended(int i) {
        if (i == CAUSE_SERVICE_DISCONNECTED) {
            Toast.makeText(this, "Déconnecté. Veuillez vous reconnecter.", Toast.LENGTH_SHORT).show();
        } else if (i == CAUSE_NETWORK_LOST) {
            Toast.makeText(this, "Réseau perdu.  Veuillez vous reconnecter.", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        if(membreCourant_ != null){
            membreCourant_.setLocation(Converter.converter_.locationToLatLng(location));
            miseAjourPosition(location);
        }

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    /**
     * parametrer les requetes de positions
     */
    protected void createLocationRequest() {
        mLocationRequest_ = new LocationRequest();
        mLocationRequest_.setInterval(MapsActivity.INTERVAL);
        mLocationRequest_.setFastestInterval(MapsActivity.FASTEST_INTERVAL);
        mLocationRequest_.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    /**
     * débuter les mises à jour des positions
     */
    @TargetApi(Build.VERSION_CODES.M)
    protected void startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(this,Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(this,Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            PendingResult<Status> pendingResult = LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient_, mLocationRequest_, this);
        }
    }

    /**
     * on arrête les mises à jour des positions
     */
    protected void stopLocationUpdates() {
        if(mGoogleApiClient_!=null && mGoogleApiClient_.isConnected()){
            //now stop the updating the location
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient_, this);
            mGoogleApiClient_.disconnect();
        }
    }

}
