package tp3inf8405.deliverytracker.Activities.Commande;

import android.app.Activity;
import android.view.View;
import android.widget.TextView;

import com.google.firebase.database.Query;

import tp3inf8405.deliverytracker.POJO.Commande;
import tp3inf8405.deliverytracker.R;

/**
 * Project: DeliveryTracker
 * Created by TP3 on 2016-04-20.
 * Description:
 */
public class CommandeAdapter extends FirebaseListAdapter<Commande> {
   // MembreModel membreCourant_ = null;

    public CommandeAdapter(Query ref, Activity activity, int layout) {
        super(ref, Commande.class, layout, activity);
    }

    @Override
    protected void populateView(View view, Commande commande) {

      //  membreCourant_ = MainThreadBus.getDefault().getStickyEvent(MembreModel.class);

        commande.setIdFirebase(getKey(commande));
        TextView titre = (TextView) view.findViewById(R.id.title);
        TextView date = (TextView) view.findViewById(R.id.date);
        TextView etat = (TextView) view.findViewById(R.id.state);
        // titre
        titre.setText(commande.getNom_client());
        date.setText(String.valueOf(commande.getHeure()));
        etat.setText(commande.getEtat());
    }
}
