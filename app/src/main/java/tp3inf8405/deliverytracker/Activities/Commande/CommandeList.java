package tp3inf8405.deliverytracker.Activities.Commande;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import mehdi.sakout.fancybuttons.FancyButton;
import tp3inf8405.deliverytracker.Activities.MapsActivity;
import tp3inf8405.deliverytracker.Bus.MainThreadBus;
import tp3inf8405.deliverytracker.Config.Utils;
import tp3inf8405.deliverytracker.Converters.Converter;
import tp3inf8405.deliverytracker.DB.DeliveryTrackerDAO;
import tp3inf8405.deliverytracker.DB.MembreModel;
import tp3inf8405.deliverytracker.POJO.Commande;
import tp3inf8405.deliverytracker.Panier.home.CommandeActivity;
import tp3inf8405.deliverytracker.R;

/**
 * Project: DeliveryTracker
 * Created by TP3 on 2016-03-14.
 * Description:
 */
public class CommandeList extends AppCompatActivity {

    private ListView listView_;
    private CommandeAdapter commandeAdpater_;
    private MembreModel membreCourant_;
    private DatabaseReference ref_;
    private Query query_ = null;
    private FancyButton btnAjouterCommande;
    //BD
    DeliveryTrackerDAO profilDAO_;
    //FirebaseAuth  mAuthData = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.votelist_activity);
       // mAuthData = MainThreadBus.getDefault().getStickyEvent(FirebaseAuth.class);


        listView_ = (ListView) findViewById(R.id.list);
        View empty = findViewById(android.R.id.empty);
        //BD locale
        profilDAO_ = new DeliveryTrackerDAO(this);
        membreCourant_ = null;

        if(profilDAO_.getAllMembre() != null )
            membreCourant_ = profilDAO_.getAllMembre().get(0);//MainThreadBus.getDefault().getStickyEvent(MembreModel.class);

        if (membreCourant_ != null){
            MainThreadBus.getDefault().postSticky(membreCourant_);
            //FireBase
            ref_ = FirebaseDatabase.getInstance().getReference().child("deliverytracker");
            query_ = ref_.child("commandes")
                    .startAt(membreCourant_.getIdFacebook())
                    .endAt(membreCourant_.getIdFacebook())
                    .orderByChild("facebook_id_client");
            query_.keepSynced(true);

            //2-
            commandeAdpater_ =  new CommandeAdapter(query_,this,R.layout.list_row);
            listView_.setAdapter(commandeAdpater_);

        } else {
            Toast.makeText(this, "Vous devez vous connecter pour voir vos commandes. Merci ", Toast.LENGTH_SHORT).show();
            finish();
            return;
        }
        if(commandeAdpater_.getCount() == 0)
            listView_.setEmptyView(empty);

        //losrsqu'on clique sur un item de la liste
        listView_.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            // on charge une nouvelle page en selectionnant un élément de la liste
            Commande commande =  (Commande) parent.getItemAtPosition(position);
            if(commande.getEtat().equals("En cours de livraison")) { // on affiche la position du livreur sur la carte
                dialogBox((Commande) parent.getItemAtPosition(position));
            }
            else if(commande.getEtat().equals("En attente"))
                Toast.makeText(getApplicationContext(), membreCourant_.getNom()+", aucun livreur n'a pris votre commande pour le moment, veuillez patienter. ", Toast.LENGTH_SHORT).show();
            else{
                Toast.makeText(getApplicationContext(), "Cette commande a déjà été livrée ", Toast.LENGTH_SHORT).show();
            }
            }
        });
        //on longpress
        listView_.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view,
                                           int position, long id) {
                Commande commande =  (Commande) parent.getItemAtPosition(position);
                dialogBox(commande);
                return true;
            }
        });

        //boutton ajouter
        btnAjouterCommande = (FancyButton)findViewById(R.id.btn_ajouter);
        btnAjouterCommande.setRadius(10);
        bouttonAjouterCommande();
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        return super.onContextItemSelected(item);
    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    /**
     * boite de dialogue qui s'affiche lorsqu'on longpress sur un evenement
     * permet d'afficher les détails d'une commande
     */
    void dialogBox(final Commande commande){
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        final TextView command = new TextView(this.getApplicationContext());
        command.setTextColor(Color.BLACK);

        builder.setMessage(commande.getDetails());

        builder.setView(command);
        //si la commande n'est pas livré on ne peut la supprimer
        if(commande.getEtat().equals("Livré")) {
            builder.setTitle("Livraison terminée:");
            String details = "";
            LatLng positionClient = Converter.converter_.stringToLatLng(commande.getPosition_client());
            details +="Adresse de destination: "+ Utils.getCompleteAddressString(positionClient.latitude, positionClient.longitude, this)+"\n\n";
            details += "Nom du livreur: " + commande.getNom_livreur();
            builder.setMessage(details + commande.getDetails());
          /*  builder.setPositiveButton("Supprimer", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    dialog.cancel();
                    //suppression
                    Firebase suppression = new Firebase("https://glowing-inferno-8938.firebaseio.com/deliverytracker/commandes/" + commande.getIdFirebase());
                    suppression.keepSynced(true);
                    suppression.removeValue();
                    Toast.makeText(getApplicationContext(), "Cette commande a été supprimée de l'historique", Toast.LENGTH_SHORT).show();
                }
            });*/
            builder.setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    dialog.cancel();
                }
            });
        }
        else if(commande.getEtat().equals("En cours de livraison")){
            String details = "";
            LatLng positionClient = Converter.converter_.stringToLatLng(commande.getPosition_client());
            details +="Adresse de destination: "+ Utils.getCompleteAddressString(positionClient.latitude, positionClient.longitude, this)+"\n\n";
            details += "Nom du livreur: " + commande.getNom_livreur()+"\n\n";
            builder.setMessage(details + commande.getDetails());
            builder.setPositiveButton("Afficher la position du livreur", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    MainThreadBus.getDefault().postSticky(commande);
                    Intent i = new Intent(getApplicationContext(), MapsActivity.class);
                    startActivity(i);
                }
            });
            builder.setNegativeButton("Annuler", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    dialog.cancel();
                }
            });
            builder.setNeutralButton("Confirmer reception", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    dialog.cancel();
                    DatabaseReference editer = FirebaseDatabase.getInstance().getReference()
                            .child("commandes").child(commande.getIdFirebase());
                    editer.keepSynced(true);
                    editer.child("etat").setValue("Livré");
                }
            });
        }
        else{
            builder.setTitle("Détails:");
            String details = "";
            LatLng positionClient = Converter.converter_.stringToLatLng(commande.getPosition_client());
            details +="Adresse de destination: "+ Utils.getCompleteAddressString(positionClient.latitude, positionClient.longitude, this)+"\n\n";
            details += "Nom du livreur: " + commande.getNom_livreur();
            builder.setMessage(details + commande.getDetails());
            builder.setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    dialog.cancel();
                }
            });
        }

        AlertDialog alert = builder.create();
        alert.show();
    }

    void bouttonAjouterCommande(){
        btnAjouterCommande.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(CommandeList.this, CommandeActivity.class);
                CommandeList.this.startActivity(myIntent);
            }
        });
    }

    @Override
    public void onResume(){
        super.onResume();
    }
}
