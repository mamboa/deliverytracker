package tp3inf8405.deliverytracker.Activities;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.maps.android.ui.IconGenerator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import mehdi.sakout.fancybuttons.FancyButton;
import tp3inf8405.deliverytracker.Bus.MainThreadBus;
import tp3inf8405.deliverytracker.Config.PlayServicesUtils;
import tp3inf8405.deliverytracker.Config.Utils;
import tp3inf8405.deliverytracker.Converters.Converter;
import tp3inf8405.deliverytracker.DB.DeliveryTrackerDAO;
import tp3inf8405.deliverytracker.DB.MembreModel;
import tp3inf8405.deliverytracker.POJO.Commande;
import tp3inf8405.deliverytracker.R;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback,
        LocationListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    private static final String TAG = "MapActivity";
    private GoogleMap googleMap_;
    LocationManager mLocationManager_;
    LocationRequest mLocationRequest_;
    GoogleApiClient mGoogleApiClient_;

    public static final long INTERVAL = 1000 * 30; //30 secondes
    public static final long FASTEST_INTERVAL = 1000 * 60 * 1; // 1 minute

    ArrayList<Marker> listeMarker_;

    boolean modeChargement_ = false;
    Commande commande = null;
    private FancyButton btnLivreur;

    //BD locale
    DeliveryTrackerDAO dbDAO_= null;
    //Firebase (BD en ligne)
    DatabaseReference ref_;
    MembreModel membreCourant_;
    //liste des utilisateurs du même groupe que l'utilisateur courant
    ArrayList<Commande> listeCommandes = null;
    FirebaseAuth mAuthData = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate ...............................");
        mAuthData = MainThreadBus.getDefault().getStickyEvent(FirebaseAuth.class);
        commande = MainThreadBus.getDefault().getStickyEvent(Commande.class);

        // permet de savoir si googleplay est disponible, montre une erreur au cas echeant
        PlayServicesUtils.checkGooglePlaySevices(this);

        mLocationManager_ = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        // ATTENTION: This "addApi(AppIndex.API)"was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        mGoogleApiClient_ = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(AppIndex.API).build();
        //pour avoir accès à la bd
        dbDAO_ = new DeliveryTrackerDAO(this.getApplicationContext());
        //initialisation de Firebase
        ref_ = FirebaseDatabase.getInstance().getReference().child("deliverytracker");
        membreCourant_ = null;

        listeCommandes = new ArrayList<Commande>();
        listeMarker_ = new ArrayList<Marker>();

        setContentView(R.layout.activity_maps);
        btnLivreur = (FancyButton) findViewById(R.id.livreur);
        btnLivreur.setRadius(10);
        btnLivreur.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                centerMapOnMyLocation();
            }
        });
        setUpMapIfNeeded();
    }

    @Override
    public void onMapReady(GoogleMap map) {
        googleMap_ = map;
        setUpMap();
    }

    @Override
    public void onStart() {
        super.onStart();
        //on connecte le client
        mGoogleApiClient_.connect();
        ArrayList<MembreModel> membres = new ArrayList<MembreModel>();
        membres.addAll(dbDAO_.getAllMembre());
        if (membres.size() > 0) {
            membreCourant_ = new MembreModel(membres.get(0));
        } else {
            finish();
            return;
        }
        ecouteBdEnligne();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setUpMapIfNeeded();
    }

    @Override
    protected void onStop() {
        stopLocationUpdates();
        // on deconnecte le client
        mGoogleApiClient_.disconnect();
        super.onStop();
    }
    /**
     * Sets up the map if it is possible to do so (i.e., the Google Play services APK is correctly
     * installed) and the map has not already been instantiated.. This will ensure that we only ever
     * call {@link #setUpMap()} once when {@link #googleMap_} is not null.
     * <p/>
     * If it isn't installed {@link SupportMapFragment} (and
     * {@link MapView MapView}) will show a prompt for the user to
     * install/update the Google Play services APK on their device.
     * <p/>
     * A user can return to this FragmentActivity after following the prompt and correctly
     * installing/updating/enabling the Google Play services. Since the FragmentActivity may not
     * have been completely destroyed during this process (it is likely that it would only be
     * stopped or paused), {@link #onCreate(Bundle)} may not be called again so we should call this
     * method in {@link #onResume()} to guarantee that it will be called.
     */
    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (googleMap_ == null) {
            // Try to obtain the map from the SupportMapFragment.
            SupportMapFragment mapFragment = (SupportMapFragment)getSupportFragmentManager().findFragmentById(R.id.map);
            mapFragment.getMapAsync(this);
            // Check if we were successful in obtaining the map.
            if (googleMap_ != null) {
                setUpMap();
            }
        }
    }

    /**
     * This is where we can add markers or lines, add listeners or move the camera. In this case, we
     * just add a marker near Africa.
     * <p/>
     * This should only be called once and when we are sure that {@link #googleMap_} is not null.
     */
    @TargetApi(Build.VERSION_CODES.M)
    private void setUpMap() {
        centerMapOnMyLocation();
        googleMap_.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        if (ActivityCompat.checkSelfPermission(this,Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(this,Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            googleMap_.setMyLocationEnabled(true);
        }
        googleMap_.setTrafficEnabled(false);
        googleMap_.setIndoorEnabled(false);
        googleMap_.setBuildingsEnabled(false);
        googleMap_.getUiSettings().setCompassEnabled(true);
        googleMap_.getUiSettings().setZoomControlsEnabled(true);
    }


    /**
     * centerMapOnMyLocation()
     * centrer la carte sur à la postion de l'utilisateur
     */
    @TargetApi(Build.VERSION_CODES.M)
    private void centerMapOnMyLocation() {
        GoogleApiAvailability googleAPI = GoogleApiAvailability.getInstance();
        Location location = null;
        int resultCode = googleAPI.isGooglePlayServicesAvailable(this);
        if (resultCode == ConnectionResult.SUCCESS){
            if (ActivityCompat.checkSelfPermission(this,Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                    || ActivityCompat.checkSelfPermission(this,Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                 location = getPermissionLocation(LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient_));
            }
            if(Converter.converter_.stringToLatLng(commande.getPosition_livreur())!= null)
                location = Converter.converter_.latlngToLocation(Converter.converter_.stringToLatLng(commande.getPosition_livreur()));
            else if(location == null)
                location = getLastBestLocation();
            LatLng myLocation = Converter.converter_.locationToLatLng(location);
            googleMap_.animateCamera(CameraUpdateFactory.newLatLngZoom(myLocation,15));
        }
    }

    /**
     * getlastBestLocation()
     * @return la dernière position
     */
    @TargetApi(Build.VERSION_CODES.M)
    private Location getLastBestLocation() {
        Location locationGPS = null;
        Location locationNet = null;
        if (mLocationManager_ != null) {
            if (ActivityCompat.checkSelfPermission(this,Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                    || ActivityCompat.checkSelfPermission(this,Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                locationGPS = mLocationManager_.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                locationNet = mLocationManager_.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            }
        }

        long GPSLocationTime = 0;
        if (null != locationGPS) { GPSLocationTime = locationGPS.getTime(); }

        long NetLocationTime = 0;

        if (null != locationNet) {
            NetLocationTime = locationNet.getTime();
        }

        if ( 0 < GPSLocationTime - NetLocationTime ) {
            return locationGPS;
        }
        else {
            return locationNet;
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        createLocationRequest();
        startLocationUpdates();
        //on met à jour la base de donnée
        //on met à jour FireBase
    }

    @Override
    public void onConnectionSuspended(int i) {
        if (i == CAUSE_SERVICE_DISCONNECTED) {
            Toast.makeText(this, "Déconnecté. Veuillez vous reconnecter.", Toast.LENGTH_SHORT).show();
        } else if (i == CAUSE_NETWORK_LOST) {
            Toast.makeText(this, "Réseau perdu.  Veuillez vous reconnecter.", Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * la  nouvelle postion de l'utilisateur , elle est contenue dans la variable location
     * @param location
     */
    @Override
    public void onLocationChanged(Location location) {
        if(membreCourant_ != null){
            LatLng updateLocation = Converter.converter_.locationToLatLng(location);
            membreCourant_.setLocation(Converter.converter_.locationToLatLng(location));
            miseAjourPosition(location);
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    /**
     * parametrer les requetes de positions
     */
    protected void createLocationRequest() {
        mLocationRequest_ = new LocationRequest();
        mLocationRequest_.setInterval(INTERVAL);
        mLocationRequest_.setFastestInterval(FASTEST_INTERVAL);
        mLocationRequest_.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    /**
     * débuter les mises à jour des positions
     */
    @TargetApi(Build.VERSION_CODES.M)
    protected void startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(this,Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(this,Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            PendingResult<Status> pendingResult = LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient_, mLocationRequest_, this);
        }
        Log.d(TAG, "Debut des requetes: ");
    }

    /**
     * on arrête les mises à jour des positions
     */
    protected void stopLocationUpdates() {
        if(mGoogleApiClient_!=null && mGoogleApiClient_.isConnected()){
            //now stop the updating the location
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient_, this);
            mGoogleApiClient_.disconnect();
            Log.d(TAG, "arret des requetes");
        }
    }

    /**
     * Mise à jour des deux BD
     */
    void miseAjourPosition(Location location){
        membreCourant_.setLocation(Converter.converter_.locationToLatLng(location));

        //on met à jour la base de données locale
        dbDAO_.update(membreCourant_);

        //on met à jour FireBase (la base de données en ligne)

        DatabaseReference ref = ref_.child("users").child(membreCourant_.getIdFirebase());
        ref.keepSynced(true);
        if(commande!= null)
        {
            DatabaseReference com = ref_.child("commandes").child(commande.getIdFirebase());
            com.child("position_client").setValue(Converter.converter_.latlongToString(Converter.converter_.locationToLatLng(location)));
        }

        // mise à jour de la position
        Map<String, Object> updateUser = new HashMap<String,Object>();
        updateUser.put("position", Converter.converter_.latlongToString(Converter.converter_.locationToLatLng(location)));
        ref.updateChildren(updateUser);
    }

    /**
     * mets à jour la position du client
     * @return
     */
    public boolean obtenirNouvellePosition() {
        Query recherche = ref_.child("commandes").child(commande.getIdFirebase())
                .orderByChild("details");
        int test = 0;
        recherche.keepSynced(true);
        recherche.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // Si la solution contient plusieurs enregistrements on les parcour
                if(dataSnapshot.getValue() != null) {
                    reinitialiserPositions();
                    modeChargement_ = true;
                    listeCommandes.add(dataSnapshot.getValue(Commande.class));

                    for(Commande com:listeCommandes)
                        placerUnMarqueur(com);
                    commande.setPosition_livreur(listeCommandes.get(0).getPosition_livreur());
                    commande.setPosition_client(listeCommandes.get(0).getPosition_client());
                }
            }

            @Override
            public void onCancelled(DatabaseError firebaseError) {
            }
        });
        return true;
    }

    /**
     * placer un marqueur à la position de l'utilisateur
     * @param commande
     */
    private void placerUnMarqueur(Commande commande) {
        LatLng location  = Converter.converter_.stringToLatLng(commande.getPosition_livreur());
        MarkerOptions options = new MarkerOptions();
        IconGenerator iconFactory = new IconGenerator(this);
        iconFactory.setStyle(IconGenerator.STYLE_BLUE); //couleur aleatoire
        options.icon(BitmapDescriptorFactory.fromBitmap(iconFactory.makeIcon(commande.getNom_livreur())));
        options.anchor(iconFactory.getAnchorU(), iconFactory.getAnchorV());
        options.position(location);
        Marker marker = googleMap_.addMarker(options);
        marker.setTitle(Utils.getCompleteAddressString(location.latitude, location.longitude, this)+"\n");
        //on sauvegarde une référence sur le marqueur
        listeMarker_.add(marker);
    }

    /**
     * ecoute le serveur en ligne et met à jour les positions du livreur
     */
    public void ecouteBdEnligne(){
        if(commande!= null) {
            ref_.child("commandes").child(commande.getIdFirebase()).addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                    obtenirNouvellePosition();
                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                    obtenirNouvellePosition();
                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {
                    obtenirNouvellePosition();
                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onCancelled(DatabaseError firebaseError) {

                }
            });
        }
    }

    /**
     * efface les marqueur et vide la liste des membres
     */
    void reinitialiserPositions(){
        for(Marker marker: listeMarker_)
            marker.remove();
        listeCommandes.clear();
    }

    @TargetApi(23)
    public Location getPermissionLocation(Location location){
        if (ActivityCompat.checkSelfPermission(this,Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(this,Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            return location;
        }
        return null;
    }
}