package tp3inf8405.deliverytracker.Activities;


/**
 * Project: DeliveryTracker
 * Created by TP3 on 2016-03-02.
 * Description:
 */

import android.app.ProgressDialog;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import mehdi.sakout.fancybuttons.FancyButton;
import tp3inf8405.deliverytracker.Activities.Commande.CommandeList;
import tp3inf8405.deliverytracker.Activities.Commande.CommandeListLivreur;
import tp3inf8405.deliverytracker.Activities.Commande.MesLivraisons;
import tp3inf8405.deliverytracker.Activities.Profil.Profile;
import tp3inf8405.deliverytracker.Bus.MainThreadBus;
import tp3inf8405.deliverytracker.Config.Utils;
import tp3inf8405.deliverytracker.Converters.Converter;
import tp3inf8405.deliverytracker.DB.DeliveryTrackerDAO;
import tp3inf8405.deliverytracker.DB.MembreModel;
import tp3inf8405.deliverytracker.Panier.home.CommandeActivity;
import tp3inf8405.deliverytracker.R;


public class MenuActivity extends AppCompatActivity {
    private static final String TAG = MenuActivity.class.getSimpleName();
    //Todo: modifier cette partie (les noms et les utilités)
    private FancyButton btnProfil_;
    private FancyButton btnPositions_;
    private FancyButton btnOptions;
    private FancyButton btnCommande;
    private FancyButton btnLivraisions;
    private FancyButton btnMesLivraisons;
    private MembreModel membreCourant_;
    //boîte de progress
    private ProgressDialog mAuthProgressDialog;
    //lien de la page Firebase
    private DatabaseReference mFirebaseRef;
    //lien de travail vers firebase
    DatabaseReference firebase_;
    String idFirebase_ = "";

    //donnees d'authentification
    private FirebaseAuth mAuthData;
    //ecoute l'etat d'une session Firebase
    private FirebaseAuth.AuthStateListener mAuthStateListener;
    private TextView mLoggedInStatusTextView;

    //par defaut on suppose que le profil existe localement
    boolean existeLocalement_ = true;
    //-------- Facebook ------
    private LoginButton btnFacebookLoginButton;
    //gestionnaire de callback pour facebook
    private CallbackManager mFacebookCallbackManager;
    //traqué l'etat de la connexion de l'utilisateur
    private AccessTokenTracker mFacebookAccessTokenTracker;
    //-------- Facebook fin ------

    DeliveryTrackerDAO profilDAO_;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        firebase_ = FirebaseDatabase.getInstance().getReference().child("deliverytracker");
        firebase_.keepSynced(true);

        setContentView(R.layout.menu_activity);
        btnProfil_ = (FancyButton)findViewById(R.id.profil);
        btnProfil_.setRadius(10);

        btnMesLivraisons = (FancyButton) findViewById(R.id.meslivraisons);
        btnMesLivraisons.setRadius(10);

        btnPositions_ = (FancyButton) findViewById(R.id.position_membres);
        btnPositions_.setVisibility(View.GONE);

        btnLivraisions = (FancyButton)findViewById(R.id.livraisons);
        btnLivraisions.setRadius(10);

        btnOptions = (FancyButton) findViewById(R.id.commandes);
        btnOptions.setRadius(10);

        btnCommande = (FancyButton) findViewById(R.id.commande);
        btnCommande.setRadius(10);

        //acceder à la page profil
        btnProfil_.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(MenuActivity.this, Profile.class);
                MenuActivity.this.startActivity(myIntent);
            }
        });



        btnCommande.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 Intent myIntent = new Intent(MenuActivity.this, CommandeActivity.class);
                 MenuActivity.this.startActivity(myIntent);
            }
        });

        btnOptions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(MenuActivity.this, CommandeList.class);
                MenuActivity.this.startActivity(myIntent);
            }
        });

        btnMesLivraisons.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(MenuActivity.this, MesLivraisons.class);
                MenuActivity.this.startActivity(myIntent);
            }
        });

        btnLivraisions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(MenuActivity.this, CommandeListLivreur.class);
                MenuActivity.this.startActivity(myIntent);
            }
        });

        membreCourant_ = null;
        profilDAO_ = new DeliveryTrackerDAO(this.getApplicationContext());

        //on recupere les donnees de facebook
        mFacebookCallbackManager = CallbackManager.Factory.create();
        btnFacebookLoginButton = (LoginButton) findViewById(R.id.login_with_facebook);

        btnFacebookLoginButton.setReadPermissions(Arrays.asList(
                "public_profile", "email"));

        btnFacebookLoginButton.registerCallback(mFacebookCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                // App code
            }

            @Override
            public void onCancel() {
                // App code
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
            }
        });
        mFacebookAccessTokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken, AccessToken currentAccessToken) {
                Log.i(TAG, "Facebook.AccessTokenTracker.OnCurrentAccessTokenChanged");
                MenuActivity.this.onFacebookAccessTokenChange(currentAccessToken);
            }
        };

        // on fait un lien à firebase en ligne
        mFirebaseRef = FirebaseDatabase.getInstance().getReference();
        mAuthData = FirebaseAuth.getInstance();
        // status de connexion
        mLoggedInStatusTextView = (TextView) findViewById(R.id.login_status);

        // on définit la boîte de dialog pour le chargement
        mAuthProgressDialog = new ProgressDialog(this);
        mAuthProgressDialog.setTitle("Chargement");
        mAuthProgressDialog.setMessage("Authentification...");
        mAuthProgressDialog.setCancelable(false);
        mAuthProgressDialog.show();



        FirebaseAuth mAuthStateListener = FirebaseAuth.getInstance();
        mAuthStateListener.addAuthStateListener(new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull final FirebaseAuth firebaseAuth) {
                final FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    Log.i("AuthStateChanged", "User is signed in with uid: " + user.getUid());
                   // user.getProviderData().
                    mAuthProgressDialog.hide();
                    setAuthenticatedUser(firebaseAuth);
                } else {
                    Log.i("AuthStateChanged", "No user is signed in.");
                }
            }
        });
    }

    private void onFacebookAccessTokenChange(AccessToken token) {
        Log.d(TAG, "handleFacebookAccessToken:" + token);
        // ...
        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        mAuthData.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d(TAG, "signInWithCredential:onComplete:" + task.isSuccessful());

                        // If sign in fails, display a message to the user. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in user can be handled in the listener.
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "signInWithCredential", task.getException());
                            Toast.makeText(MenuActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                            if(membreCourant_ != null) {
                                profilDAO_.delete(membreCourant_);
                            }
                        }
                    }
                });
    }

    @Override
    public void onStart() {
        super.onStart();
        /* Check if the user is authenticated with Firebase already. If this is the case we can set the authenticated
         * user and hide hide any login buttons */
        mAuthData.addAuthStateListener(mAuthStateListener);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mAuthStateListener != null) {
            mAuthData.removeAuthStateListener(mAuthStateListener);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // if user logged in with Facebook, stop tracking their token
        if (mFacebookAccessTokenTracker != null) {
            mFacebookAccessTokenTracker.stopTracking();
        }

        if (mAuthStateListener != null) {
            mAuthData.removeAuthStateListener(mAuthStateListener);
        }
    }


    /**
     * This method fires when any startActivityForResult finishes. The requestCode maps to
     * the value passed into startActivityForResult.
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Map<String, String> options = new HashMap<String, String>();
        /* Otherwise, it's probably the request by the Facebook login button, keep track of the session */
        mFacebookCallbackManager.onActivityResult(requestCode, resultCode, data);
    }

    /**
     * Once a user is logged in, take the mAuthData provided from Firebase and "use" it.
     */
    private void setAuthenticatedUser(FirebaseAuth authData) {
        if (authData != null) {
            /* Hide all the login buttons */
            btnFacebookLoginButton.setVisibility(View.VISIBLE);
            mLoggedInStatusTextView.setVisibility(View.VISIBLE);
            /* show a provider specific status text */
            String name = null;

            if (authData.getCurrentUser().getProviderId().equals("facebook.com")
                    || authData.getCurrentUser().getProviderId().equals("google.com")
                    || authData.getCurrentUser().getProviderId().equals("twitter.com")) {
                name = (String) authData.getCurrentUser().getDisplayName();
            } else if (authData.getCurrentUser().getProviderId().equals("anonymous")
                    || authData.getCurrentUser().equals("password")) {
                name = authData.getCurrentUser().getUid();
            } else {
                Log.e(TAG, "Invalid provider: " + authData.getCurrentUser().getProviderId());
            }
            if (name != null) {
                mLoggedInStatusTextView.setText("Logged in as " + name + " (" + authData.getCurrentUser().getProviderId() + ")");
                int test = 0;
            }
        } else {
            /* No authenticated user show all the login buttons */
            btnFacebookLoginButton.setVisibility(View.VISIBLE);

         /*   mGoogleLoginButton.setVisibility(View.VISIBLE);
            mTwitterLoginButton.setVisibility(View.VISIBLE);
            mPasswordLoginButton.setVisibility(View.VISIBLE);
            mAnonymousLoginButton.setVisibility(View.VISIBLE);*/
            mLoggedInStatusTextView.setVisibility(View.GONE);
        }
        this.mAuthData = authData;
        if(mAuthData != null ) {
            MainThreadBus.getDefault().postSticky(mAuthData);
            ajouterProfil(mAuthData);
        }

        /* invalidate options menu to hide/show the logout button */
        this.supportInvalidateOptionsMenu();
    }

    public void ajouterProfil(FirebaseAuth profilFacebook){
        existeLocalement_ = true;

        membreCourant_ = chargerFacebookLocalement(profilFacebook);  //on copie les informations de facebook
        if (!profilDAO_.siMembreExiste((String)profilFacebook.getCurrentUser().getUid()))
            existeLocalement_ = false;

        ajoutSiMembreExisteEnligne();   //on ajoute à firebase
    }

    public MembreModel chargerFacebookLocalement(FirebaseAuth profilFacebook){
        MembreModel membre = new MembreModel();
        membre.setIdFacebook((String)profilFacebook.getCurrentUser().getUid())
              .setCourriel((String)profilFacebook.getCurrentUser().getEmail())
              .setLienImage((String)profilFacebook.getCurrentUser().getPhotoUrl().toString())
              .setNom((String)profilFacebook.getCurrentUser().getDisplayName())
              .setLocation(Converter.converter_.locationToLatLng(Utils.getLastKnownLoaction(true, this)));
        return membre;
    }

    /**
     * teste si le groupe existe en ligne avant de l'ajouter
     * @return
     */
    public boolean ajoutSiMembreExisteEnligne() {
        boolean ajoutOk_  = true;
        boolean existe = false;
        idFirebase_ = "";
        String idFacebook = membreCourant_.getIdFacebook();

        // Select * from delivery.users where facebook = idFacebook
        Query recherche =  firebase_.child("users")
                .startAt(idFacebook)
                .endAt(idFacebook).orderByChild("facebook_id");
        recherche.keepSynced(true);
        recherche.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // Si la solution contient plusieurs enregistrements on les parcourt
                if(dataSnapshot != null) {
                    for (DataSnapshot child : dataSnapshot.getChildren())
                        idFirebase_ = child.getKey();
                }

                if(!idFirebase_.equals(""))
                    membreCourant_.setIdFirebase(idFirebase_);

                ajouterProfil(membreCourant_);
            }

            @Override
            public void onCancelled(DatabaseError firebaseError) {
            }
        });
        return ajoutOk_;
    }

    /**
     * Ajoute un profil en local et en ligne selon qu'il est necessaire
     * @param membre
     * @return
     */
    public void ajouterProfil(MembreModel membre){
        //le profil n'existe pas en ligne
        if(membre.getIdFirebase().equals("")){
            //sauvegarde en ligne
            DatabaseReference lienEnligne = firebase_.child("users");
            lienEnligne.keepSynced(true);
            DatabaseReference membre_push = lienEnligne.push(); //creation d'une branche pour un nouveau membre
            membre_push.keepSynced(true);
            Map<String, String> leMembre = new HashMap<String, String>();
            leMembre.put("nom", membre.getNom());
            leMembre.put("facebook_id",membre.getIdFacebook());
            leMembre.put("lien_image", membre.getLienImage());
            leMembre.put("position",Converter.converter_.latlongToString(membre.getLocation()));
            leMembre.put("email", membre.getCourriel());
            membre_push.setValue(leMembre);             //sauvegarde en ligne


            membre.setIdFirebase(membre_push.getKey()); //on recupere l'id généré en ligne par FireBase
        }
        if(existeLocalement_)
            profilDAO_.update(membre);
        else
            profilDAO_.insert(membre);
        //on met à jour la position
        miseAjourPosition(Utils.getLastKnownLoaction(true, this));
        MainThreadBus.getDefault().postSticky(membreCourant_);
    }

    /**
     * Mise à jour des deux BD: position uniquement
     */
    void miseAjourPosition(Location location) {
        membreCourant_.setLocation(Converter.converter_.locationToLatLng(location));

        //on met à jour la base de données locale
        profilDAO_.update(membreCourant_);

        //on met à jour FireBase (la base de données en ligne)
        // racine -> group -> postGroupId -> user -> membreId
        DatabaseReference ref = firebase_.child("users").child(membreCourant_.getIdFirebase());

        // mise à jour de la position
        Map<String, Object> updateUser = new HashMap<String,Object>();
        updateUser.put("position", Converter.converter_.latlongToString(Converter.converter_.locationToLatLng(location)));
        ref.updateChildren(updateUser);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
/* If a user is currently authenticated, display a logout menu */
        if (this.mAuthData != null) {
            // Add the filter & search buttons to the toolbar.
            getMenuInflater().inflate(R.menu.main, menu);
            return true;
        } else {
            return false;
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_logout) {
            logout();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Unauthenticate from Firebase and from providers where necessary.
     */
    private void logout() {
        if (this.mAuthData != null) {
        /* logout of Firebase */
            FirebaseAuth.getInstance().signOut();
            /* Logout of any of the Frameworks. This step is optional, but ensures the user is not logged into
            * Facebook/Google+ after logging out of Firebase. */
            if (this.mAuthData.getCurrentUser().getProviderId().equals("facebook.com")) {
                /* Logout from Facebook */
                LoginManager.getInstance().logOut();
                this.mAuthData = null;
                MainThreadBus.getDefault().postSticky(this.mAuthData);
                if(membreCourant_ != null) {
                    profilDAO_.delete(membreCourant_);
                    MainThreadBus.getDefault().postSticky(this.membreCourant_);
                }
            }
            setAuthenticatedUser(null);
        }
    }



}

