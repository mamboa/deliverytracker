package tp3inf8405.deliverytracker.POJO;

/**
 * Project: DeliveryTracker
 * Created by TP3 on 2016-04-27.
 * Description:
 */
public class Commandes {

    String facebook_id_client;
    String nom_client;
    String position_client;
    String heure_depart;
    String heure;
    String facebook_id_livreur;
    String nom_livreur;
    String position_livreur;
    String details;
    String montant;
    String idFirebase;
    String etat; //en attente / en cours de livraison/ livré

    public Commandes() {
    }

    public String getFacebook_id_client() {
        return facebook_id_client;
    }

    public void setFacebook_id_client(String facebook_id_client) {
        this.facebook_id_client = facebook_id_client;
    }

    public String getHeure_depart() {
        return heure_depart;
    }

    public void setHeure_depart(String heure_depart) {
        this.heure_depart = heure_depart;
    }

    public String getPosition_client() {
        return position_client;
    }

    public void setPosition_client(String position_client) {
        this.position_client = position_client;
    }

    public String getFacebook_id_livreur() {
        return facebook_id_livreur;
    }

    public void setFacebook_id_livreur(String facebook_id_livreur) {
        this.facebook_id_livreur = facebook_id_livreur;
    }

    public String getPosition_livreur() {
        return position_livreur;
    }

    public void setPosition_livreur(String position_livreur) {
        this.position_livreur = position_livreur;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getMontant() {
        return montant;
    }

    public void setMontant(String montant) {
        this.montant = montant;
    }

    public String getIdFirebase() {
        return idFirebase;
    }

    public void setIdFirebase(String idFirebase) {
        this.idFirebase = idFirebase;
    }

    public String getNom_client() {
        return nom_client;
    }

    public void setNom_client(String nom_client) {
        this.nom_client = nom_client;
    }

    public String getHeure() {
        return heure;
    }

    public void setHeure(String heure) {
        this.heure = heure;
    }

    public String getNom_livreur() {
        return nom_livreur;
    }

    public void setNom_livreur(String nom_livreur) {
        this.nom_livreur = nom_livreur;
    }

    public String getEtat() {
        return etat;
    }

    public void setEtat(String etat) {
        this.etat = etat;
    }

}
