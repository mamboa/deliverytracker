package tp3inf8405.deliverytracker.Bus;

import android.os.Handler;
import android.os.Looper;

import de.greenrobot.event.EventBus;

/**
 * Project: MeetUp
 * Created by TP2 on 2016-03-02.
 * Description:
 * More on: https://github.com/greenrobot/EventBus
 */

public class MainThreadBus extends EventBus {
    private final Handler handler = new Handler(Looper.getMainLooper());

    @Override public void post(final Object event) {
        if (Looper.myLooper() == Looper.getMainLooper()) {
            super.post(event);
        } else {
            handler.post(new Runnable() {
                @Override
                public void run() {
                    MainThreadBus.super.post(event);
                }
            });
        }
    }
}
