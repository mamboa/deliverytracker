package tp3inf8405.deliverytracker.Converters;

import android.location.Location;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.InflaterInputStream;

import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

/**
 * Project: DeliveryTracker
 * Created by TP3 on 2016-03-02.
 * Description: Quelques conversions utiles
 */
public class Converter {
    final static  public Converter converter_ = new Converter();

    public Converter(){}

    /**
     * convertir de location à  LatLng
     * @param location location class
     * @return the coordinates in Latlng
     */
    public LatLng locationToLatLng(Location location){
        if(location == null)
            return null;
        return new LatLng(location.getLatitude(),location.getLongitude());
    }

    /**
     * convert Latlng to location
     * @param latlng LatLng class
     * @return the coordinates in Location type
     */
    public Location latlngToLocation(LatLng latlng){
        if(latlng == null)
            return  null;
        //Convert LatLng to Location
        Location location = new Location("");
        location.setLatitude(latlng.latitude);
        location.setLongitude(latlng.longitude);
        location.setTime(new Date().getTime()); //Set time as current Date
        return location;
    }

    /**
     * fromResponseToString(Response response)
     * converts the http response in string
     * @param  response the http response is a string containing json
     * @return string
     */
    public String fromResponseToString(Response response){
        return new String(((TypedByteArray) response.getBody()).getBytes());
    }

    /**
     * turns an arrayList of any object into a JSON string
     * @param list list of the object
     * @return the waited string
     */
   public String fromListToString(ArrayList<?> list){
       return new Gson().toJson(list);
   }

    /**
     * fromStringToLatLngList
     * converts a string Json to an arrayList of LatLong
     * @param value the Json string to be turned in an ArrayList of latlon
     * @return an arrayList of latlong
     */
   public ArrayList<LatLng> fromStringToLatLngList(String value){
        return new Gson().fromJson(value,new TypeToken<ArrayList<LatLng>>(){}.getType());
   }

    /**
     * fromStringToStringList
     * converts a string Json to  an arrayList of string
     * @param value the Json string to be turned in an ArrayList of String
     * @return an arrayList of strings
     */
    public ArrayList<String> fromStringToStringList(String value){
        return new Gson().fromJson(value,new TypeToken<ArrayList<String>>(){}.getType());
    }

    /**
     * transfrom string into an array after removing spaces and splitting by the coma
     * @param value
     * @return
     */
    public ArrayList<String> stringToArrayList(String value){
       return new ArrayList<String>(Arrays.asList(value.replace(" ", "").split(",")));
    }
    /**
     * longTimeToStringDate()
     * turns the time initially in long to a string date with the format dd-MM-yy
     * @param time the given time in long
     * @return the date in string
     */
    public String longTimeToStringDate(long time){
        return new SimpleDateFormat("dd-MM-yy").format(new Date(time));
    }

    /**
     * longTimeToStringTime()
     * turns the time initially in long to a string time with the format
     * @param time the given time in long
     * @return the time in string
     */
    public String longTimeToStringTime(long time){
        return DateFormat.getTimeInstance().format(new Date(time));
    }

    public String latlongToString(LatLng value){
       if(value == null)
           return "";
       return value.toString();
    }

    public LatLng stringToLatLng(String value){
        if(!value.equals("")) {
            value = value.replace("lat/lng:", "");
            value = value.replace("(", "");
            value = value.replace(")", "");
            String[] latlong = value.split(",");
            double latitude = Double.parseDouble(latlong[0]);
            double longitude = Double.parseDouble(latlong[1]);
            return new LatLng(latitude, longitude);
        }
        return null;
    }

    public boolean stringToBoolean(String valeur){
        return (valeur =="true"?true:false);
    }

    public String booleanToString(boolean valeur){
        return (valeur == true?"true":"false");
    }

    //The two methods below weren't tested and aren't used for now
    /**
     * source : http://stackoverflow.com/questions/10572398/how-can-i-easily-compress-and-decompress-strings-to-from-byte-arrays
     * compress()
     * @param text the text to be compressed
     * @return the result in byte of the compressed text
     */
    public static byte[] compress(String text) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            OutputStream out = new DeflaterOutputStream(baos);
            out.write(text.getBytes("UTF-8"));
            out.close();
        } catch (IOException e) {
            throw new AssertionError(e);
        }
        return baos.toByteArray();
    }

    /**
     * source : http://stackoverflow.com/questions/10572398/how-can-i-easily-compress-and-decompress-strings-to-from-byte-arrays
     * decompress()
     * @param bytes the compressed array byte to be decompressed into a string
     * @return the decompressed result in string
     */
    public static String decompress(byte[] bytes) {
        InputStream in = new InflaterInputStream(new ByteArrayInputStream(bytes));
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            byte[] buffer = new byte[8192];
            int len;
            while((len = in.read(buffer))>0)
                baos.write(buffer, 0, len);
            return new String(baos.toByteArray(), "UTF-8");
        } catch (IOException e) {
            throw new AssertionError(e);
        }
    }

    /**
     * from name to google type
     */
    public String nameToPlaceType(String name){
        if(name.equals("Restaurant"))
            return "restaurant";
        if(name.equals("Parc"))
            return "amusement_park";
        if(name.equals("Cafétaria"))
            return "cafe";
        if(name.equals("Gym"))
            return "gym";
        if(name.equals("École"))
            return "school";
        if(name.equals("Université"))
            return "university";
        if(name.equals("Bibliothèque"))
            return "library";
        if(name.equals("Pizzeria"))
            return "restaurant";
        if(name.equals("Cinéma"))
            return "movie_theater";
        return "not found";
    }
}
